/// <reference path="app.ts" />
namespace test1 {
    export let x:string = "Hello";
}

namespace test2 {
    export let x:string = "World!";
}

namespace space {
    export let x:string = " ";
}

namespace test3 {
    export let x:number = 78;
}

