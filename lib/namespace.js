"use strict";
/// <reference path="app.ts" />
var test1;
(function (test1) {
    test1.x = "Hello";
})(test1 || (test1 = {}));
var test2;
(function (test2) {
    test2.x = "World!";
})(test2 || (test2 = {}));
var space;
(function (space) {
    space.x = " ";
})(space || (space = {}));
var test3;
(function (test3) {
    test3.x = 78;
})(test3 || (test3 = {}));
//# sourceMappingURL=namespace.js.map